const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    'bundle' : [
      './app/index.js'
    ]
  },
  devtool: "eval",
  debug: true,
  output: {
    publicPath: "http://localhost:8080/",
    filename: '[name].js'
  },
  resolveLoader: {
    modulesDirectories: ['node_modules']
  },
  resolve: {
    extensions: ['','.js']
  },
  postcss: function () {
      return [autoprefixer];
  },
  module: {
    loaders: [
      {
        test: /\.less|.css$/,
        loader: "style!raw!postcss!less"
      },
      {test: /\.js$/, loaders:['babel'], exclude: /node_modules/},
    ]
  }
};
