import React from 'react';
import './notify.less';

class Notify extends React.Component {
  render(){
    return(
      <div className={this.props.status}>
        <div className="notify">
          {this.props.msg}
        </div>
      </div>
    )
  }
}

export default Notify;
