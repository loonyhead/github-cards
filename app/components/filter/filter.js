import React from 'react';
import classNames from 'classNames';

class Filter extends React.Component {
  constructor() {
    super();
    this.state = {
      selected : ''
    };
  }
  applyFilter(type){
    this.setState({selected  : type})
    this.props.onFilter(type);
  }
  isActive(type){
    return ((type===this.state.selected) ?'active':'');
  }
  render(){
    return(
      <div className="sort-wrapper">
        Sort by:
        <span className={this.isActive('name')} onClick={this.applyFilter.bind(this, 'name')}>Name</span>
        <span className={this.isActive('location')} onClick={this.applyFilter.bind(this, 'location')}>Location</span>
        <span className={this.isActive('followers')} onClick={this.applyFilter.bind(this, 'followers')}>Followers</span>
      </div>
    )
  }
}

export default Filter;
