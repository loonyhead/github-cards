import React from 'react';
import classNames from 'classNames';

class Card extends React.Component {
  removeUser(e){
    e.stopPropagation();
    this.props.removeCard(this.props.user.id);
  }
  redirect(link){
    window.open(link, '_blank');
  }
  render(){
    const {user} = this.props;
    let loc="", name="";
    if(user.location){
      loc = (<div className="location">Location: {user.location}</div>);
    }
    if(user.name){
      name = (<div className="name">{user.name}</div>);
    }
    return(
        <li className="card" data-id={user.id} onClick={this.redirect.bind(this, user.html_url)}>
          <div className="remove" onClick={this.removeUser.bind(this)}>x</div>
          <img src={user.avatar_url} alt={user.login} />
          {name}
          {loc}
          <div className="followers">Followers: {user.followers}</div>
        </li>
    )
  }
}

export default Card;
