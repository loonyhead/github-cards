import React from 'react';
import classNames from 'classNames';

class Input extends React.Component {
  constructor() {
    super();
    this.state = {
      input : ''
    };
  }
  handleChange(e){
    this.setState({
      input : e.target.value
    });
  }
  addUser(){
    this.props.onAddUser(this.state.input);
    this.state = {input : ''};
  }
  render(){
    return(
      <div className="input">
        <input type="text" name="" value={this.state.input} className="username" onChange={(e)=>this.handleChange(e)} />
        <button type="button" name="Add" className="add" onClick={this.addUser.bind(this)}>Add</button>
      </div>
    )
  }
}

export default Input;
