import React from 'react';
import ReactDOM from 'react-dom';
import $ from "jquery";

import './index.less';
import Input from './components/input/input';
import Filter from './components/filter/filter';
import Card from './components/card/card';
import Notify from './components/notify/notify';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      cards : [],
      filter : '',
      showNotify : 'hide',
      notifyMsg : '',
    };
  }
  renderCards(){
    let {cards} = this.state;
    return cards.map((user) => {
      return <Card key={user.id} user={user} removeCard={this.removeCard.bind(this)} />
    });
  }
  onAddUser(username){
    const that = this;
    $.ajax({
      url : 'https://api.github.com/users/'+username,
      success: function(data) {
        let duplicate = false;
        that.state.cards.map((item) => {
          if(data.id == item.id){
            duplicate = true;
          }
        });
        if(!duplicate){
          let {id, login, avatar_url, html_url, name, followers, location, ...otherData} = data;
          let userData = {id, login, avatar_url, html_url, name, followers, location};
          if(that.state.filter == ''){
            that.setState({
                cards: that.state.cards.concat([userData])
            });
          }else{
            let newArr = that.state.cards;
            newArr.push(userData);
            let sortedArr = that.sortOps(newArr, that.state.filter);
            that.setState({cards: sortedArr});
          }
          that.showNotify("New UserCard Added!");
        }else {
          that.showNotify("Duplicate user not allowed! Please try again");
        }
      },
      error: function() {
        that.showNotify("Something went wrong! Please try again");
      }
    });
  }
  removeCard(index){
    let {cards} = this.state;
    let i = 0;
    cards.map((item, idx) => {
      if(item.id == index){
        i = idx;
      }
    });
    cards.splice( i ,1);
    this.setState(cards);
    this.showNotify("One UserCard has been removed!");
  }
  showNotify(msg){
    this.setState({
      "notifyMsg" : msg,
      "showNotify": "show"
    });
    setTimeout(()=> {
      this.setState({
        "notifyMsg" : "",
        "showNotify": "hide"
      });
    }, 3000);
  }
  sortOps(arr, type) {
      return arr.sort(function(a, b){
        if(a[type] === null){
          return 1;
        }
        else if(b[type] === null){
          return -1;
        }else{
          if(type != 'followers'){
            var a1 = a[type].toLowerCase();
            var b1 = b[type].toLowerCase();
            return ((a1 < b1) ? -1 : ((a1 > b1) ? 1 : 0));
          }else{
            return b[type] - a[type];
          }
        }
      });
  }
  onFilter(type){
    let sortedArr = this.sortOps(this.state.cards, type);
    this.setState({
        cards: sortedArr,
        filter : type
    });
  }
  render() {
    return (
      <div className="app">
        <Input onAddUser={this.onAddUser.bind(this)} />
        <Filter onFilter={this.onFilter.bind(this)} />
        <ul className="cards-wrapper">
          {this.state.cards.length>0? this.renderCards() : <li className="empty">No users to show. Add User!</li>}
        </ul>
        <Notify msg={this.state.notifyMsg} status={this.state.showNotify} />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
